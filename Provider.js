'use strict'

const {ServiceProvider} = require('@adonisjs/fold')
const path = require('path')

class Provider extends ServiceProvider {
  _helpers = use('Helpers')

  /**
   *
   * @param name
   * @param path
   * @return {Provider}
   */
  bindModel(name, path) {
    this.app.bind(name, () => {
      const Instance = require(this._getFullPath(path))
      Instance.boot()
      return Instance
    })
    return this
  }

  /**
   *
   * @param name
   * @param path
   * @return {Provider}
   */
  bindClass(name, path) {
    this.app.bind(name, () => {
      const Instance = require(this._getFullPath(path))
      return new Instance
    })
    return this
  }

  /**
   *
   * @param name
   * @param path
   * @return {Provider}
   */
  bindObject(name, path) {
    this.app.bind(name, () => require(this._getFullPath(path)))
    return this
  }

  /**
   *
   * @param name
   * @param relativePath
   * @return {Provider}
   */
  bindValidatorPath(name, relativePath) {
    this.app.bind(name, () => `./../../node_modules/${this.packageName}/src/Validators/${relativePath}`)
    return this
  }

  /**
   *
   * @param name
   * @return {Provider}
   */
  setPackageName(name) {
    this.packageName = name
    return this
  }

  /**
   *
   * @param pathToFile
   * @return {string}
   * @private
   */
  _getFullPath(pathToFile) {
    return path.join(this._helpers.appRoot(), 'node_modules', this.packageName, pathToFile)
  }

  /**
   *
   * @param path
   * @return {function(*=)}
   */
  handleRoute(path) {
    let [controller, method] = path.split('.')
    return (args) => {
      return use(controller)[method](args)
    }
  }
}

module.exports = Provider
